package com.example.body_mass_kotlin

enum class Morphology {
    SMALL, MEDIUM, BROAD
}