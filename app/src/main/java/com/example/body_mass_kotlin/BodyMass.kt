package com.example.body_mass_kotlin

class BodyMass {
    companion object {
        fun RFM(gender: Gender, altura: Int, cc: Int): Double {
            var result = 0.0
            if (gender === Gender.MALE) {
                result = (64 - 20 * altura / cc).toDouble()
            }
            if (gender === Gender.FEMALE) {
                result = (76 - 20 * altura / cc).toDouble()
            }
            return result
        }

        fun WV(gender: Gender, altura: Int): Double {
            var result = 0.0
            if (gender === Gender.MALE) {
                result = (altura - 150) * 0.75 + 50
            }
            if (gender === Gender.FEMALE) {
                result = (altura - 150) * 0.6 + 50
            }
            return result
        }

        fun L(gender: Gender, altura: Int, edat: Int): Double {
            var result = 0.0
            if (gender === Gender.MALE) {
                result = (altura - 100 - (altura - 150) / 4 + (edat - 20) / 4).toDouble()
            }
            if (gender === Gender.FEMALE) {
                result = altura - 100 - (altura - 150) / 2.5 + (edat - 20) / 2.5
            }
            return result
        }

        fun C(morphology: Morphology, altura: Int, edat: Int): Double {
            var result = 0.0
            if (morphology === Morphology.SMALL) {
                result = Math.round((altura - 100 + edat / 10) * Math.pow(0.9, 2.0) * 100.0) / 100.0
            }
            if (morphology === Morphology.MEDIUM) {
                result = Math.round((altura - 100 + edat / 10) * 0.9 * 100.0) / 100.0
            }
            if (morphology === Morphology.BROAD) {
                result = Math.round((altura - 100 + edat / 10) * 0.9 * 1.1 * 100.0) / 100.0
            }
            return result
        }
    }
}
