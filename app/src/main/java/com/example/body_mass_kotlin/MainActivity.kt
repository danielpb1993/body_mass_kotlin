package com.example.body_mass_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class MainActivity : AppCompatActivity() {
    private lateinit var radioGroupType: RadioGroup
    private lateinit var radioGroupGender: RadioGroup
    private lateinit var radioGroupMorph: RadioGroup

    private lateinit var radioBtFat: RadioButton
    private lateinit var radioBtWan: RadioButton
    private lateinit var radioBtLorentz: RadioButton
    private lateinit var radioBtCreff: RadioButton
    private lateinit var radioBtMale: RadioButton
    private lateinit var radioBtFemale: RadioButton
    private lateinit var radioBtSmall: RadioButton
    private lateinit var radioBtMedium: RadioButton
    private lateinit var radioBtBroad: RadioButton

    private lateinit var textHeight: TextInputEditText
    private lateinit var textWaist: TextInputEditText
    private lateinit var textAge: TextInputEditText
    private lateinit var textWaistLay: TextInputLayout
    private lateinit var textAgeLay: TextInputLayout

    private lateinit var textResult: TextView
    private lateinit var linearTable: LinearLayout

    private lateinit var btCalculate: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        radioGroupType = findViewById(R.id.radioGroupType)
        radioGroupGender = findViewById(R.id.gender)
        radioGroupMorph = findViewById(R.id.morphology)

        radioBtFat = findViewById(R.id.radioBtFat)
        radioBtWan = findViewById(R.id.radioBtWan)
        radioBtLorentz = findViewById(R.id.radioBtLorentz)
        radioBtCreff = findViewById(R.id.radioBtCreff)
        radioBtMale = findViewById(R.id.radioBtMale)
        radioBtFemale = findViewById(R.id.radioBtFemale)
        radioBtSmall = findViewById(R.id.radioBtSmall)
        radioBtMedium = findViewById(R.id.radioBtMedium)
        radioBtBroad = findViewById(R.id.radioBtBroad)

        textHeight = findViewById(R.id.textHeight)
        textWaist = findViewById(R.id.textWaist)
        textAge = findViewById(R.id.textAge)
        textWaistLay = findViewById(R.id.textWaistLay)
        textAgeLay = findViewById(R.id.textAgeLay)

        textResult = findViewById(R.id.textResult)
        linearTable = findViewById(R.id.linearTable)

        btCalculate = findViewById(R.id.btCalculate)

        textHeight.setVisibility(View.GONE)
        radioGroupGender.setVisibility(View.GONE)
        radioGroupMorph.setVisibility(View.GONE)
        textWaistLay.setVisibility(View.GONE)
        textAgeLay.setVisibility(View.GONE)
        btCalculate.setVisibility(View.GONE)
        linearTable.setVisibility(View.GONE)

        radioGroupType.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
            textHeight.setVisibility(View.VISIBLE)
            btCalculate.setVisibility(View.VISIBLE)
            var radioSelected = radioGroupType.getCheckedRadioButtonId()
            when (radioSelected) {
                R.id.radioBtFat -> {
                    linearTable.setVisibility(View.VISIBLE)
                    radioGroupGender.setVisibility(View.VISIBLE)
                    radioGroupMorph.setVisibility(View.GONE)
                    textWaistLay.setVisibility(View.VISIBLE)
                    textAgeLay.setVisibility(View.GONE)
                }
                R.id.radioBtWan -> {
                    linearTable.setVisibility(View.GONE)
                    radioGroupGender.setVisibility(View.VISIBLE)
                    radioGroupMorph.setVisibility(View.GONE)
                    textAgeLay.setVisibility(View.GONE)
                    textWaistLay.setVisibility(View.GONE)
                }
                R.id.radioBtLorentz -> {
                    linearTable.setVisibility(View.GONE)
                    radioGroupGender.setVisibility(View.VISIBLE)
                    radioGroupMorph.setVisibility(View.GONE)
                    textAgeLay.setVisibility(View.VISIBLE)
                    textWaistLay.setVisibility(View.GONE)
                }
                R.id.radioBtCreff -> {
                    linearTable.setVisibility(View.GONE)
                    radioGroupMorph.setVisibility(View.VISIBLE)
                    radioGroupGender.setVisibility(View.GONE)
                    textAgeLay.setVisibility(View.VISIBLE)
                    textWaistLay.setVisibility(View.GONE)
                }
                else -> {
                }
            }
        })
        btCalculate.setOnClickListener(View.OnClickListener {
            if (radioBtFat.isChecked() && radioBtMale.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textWaist.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opWaist = textWaist.getText().toString().toInt()
                    textResult.text =
                        (BodyMass.RFM(Gender.MALE, opHeight, opWaist).toString() + "%")
                }
            }
            if (radioBtFat.isChecked() && radioBtFemale.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textWaist.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opWaist = textWaist.getText().toString().toInt()
                    textResult.text =
                        (BodyMass.RFM(Gender.FEMALE, opHeight, opWaist).toString() + "%")
                }
            }
            if (radioBtWan.isChecked() && radioBtMale.isChecked()) {
                if (textHeight.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    textResult.text = (BodyMass.WV(Gender.MALE, opHeight).toString() + "Kg")
                }
            }
            if (radioBtWan.isChecked() && radioBtFemale.isChecked()) {
                if (textHeight.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    textResult.text = (BodyMass.WV(Gender.FEMALE, opHeight).toString() + "Kg")
                }
            }
            if (radioBtLorentz.isChecked() && radioBtMale.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textAge.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opAge = textAge.getText().toString().toInt()
                    textResult.text = (BodyMass.L(Gender.MALE, opHeight, opAge).toString() + "Kg")
                }
            }
            if (radioBtLorentz.isChecked() && radioBtFemale.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textAge.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opAge = textAge.getText().toString().toInt()
                    textResult.text = (BodyMass.L(Gender.FEMALE, opHeight, opAge).toString() + "Kg")
                }
            }
            if (radioBtCreff.isChecked() && radioBtSmall.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textAge.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opAge = textAge.getText().toString().toInt()
                    textResult.text = (BodyMass.C(Morphology.SMALL, opHeight, opAge).toString() + "Kg")
                }
            }
            if (radioBtCreff.isChecked() && radioBtMedium.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textAge.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opAge = textAge.getText().toString().toInt()
                    textResult.text = (BodyMass.C(Morphology.MEDIUM, opHeight, opAge).toString() + "Kg")
                }
            }
            if (radioBtCreff.isChecked() && radioBtBroad.isChecked()) {
                if (textHeight.text.isNullOrEmpty() || textAge.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Introdueix la informació requerida", Toast.LENGTH_SHORT).show()
                } else {
                    var opHeight = textHeight.getText().toString().toInt()
                    var opAge = textAge.getText().toString().toInt()
                    textResult.text = (BodyMass.C(Morphology.BROAD, opHeight, opAge).toString() + "Kg")
                }
            }
        })
    }
}