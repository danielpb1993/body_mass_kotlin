package com.example.body_mass_kotlin

enum class Gender {
    MALE, FEMALE
}
